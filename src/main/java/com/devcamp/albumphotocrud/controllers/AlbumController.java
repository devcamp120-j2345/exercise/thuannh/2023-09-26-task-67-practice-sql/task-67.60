package com.devcamp.albumphotocrud.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import com.devcamp.albumphotocrud.model.CAlbum;
import com.devcamp.albumphotocrud.repository.IAlbumRepository;

import java.util.*;

import javax.validation.Valid;

@RestController
@CrossOrigin
@RequestMapping("/album")
public class AlbumController {
    @Autowired
    IAlbumRepository albumRepository;
    @GetMapping("/all")
    public ResponseEntity<List<CAlbum>> getAllAlbum(){
        try {
            List<CAlbum> albumList = new ArrayList<>();
            albumRepository.findAll().forEach(albumList::add);
            return new ResponseEntity<>(albumList, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }
    
    @GetMapping("detail/{id}")
    public ResponseEntity<CAlbum> getAlbumById(@PathVariable long id){
        try {
            CAlbum album = albumRepository.findById(id);
            if (album != null){
                return new ResponseEntity<>(album, HttpStatus.OK);
            }
            else return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/create")
    public ResponseEntity<Object> createAlbum(@Valid @RequestBody CAlbum pAlbum){
        try {
            CAlbum newAlbum = new CAlbum();
            newAlbum.setCode(pAlbum.getCode());
            newAlbum.setName(pAlbum.getName());
            newAlbum.setTitle(pAlbum.getTitle());
            newAlbum.setCreated(new Date());
            newAlbum.setPhotos(pAlbum.getPhotos());
            CAlbum _album = albumRepository.save(newAlbum);
            return new ResponseEntity<>(_album, HttpStatus.CREATED);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: "+e.getCause().getCause().getMessage());
			return ResponseEntity.unprocessableEntity().body("Failed to Create specified Album: "+e.getCause().getCause().getMessage());

        }
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<CAlbum> updateAlbumById(@PathVariable("id") long id, @Valid @RequestBody CAlbum pAlbum){
        try {
            CAlbum album = albumRepository.findById(id);
            if (album != null){
                album.setCode(pAlbum.getCode());
                album.setName(pAlbum.getName());
                album.setTitle(pAlbum.getTitle());
                album.setPhotos(pAlbum.getPhotos());
                
                return new ResponseEntity<>(albumRepository.save(album), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/delete/{id}")
	public ResponseEntity<CAlbum> deleteAlbumById(@PathVariable("id") long id) {
		try {
			albumRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
