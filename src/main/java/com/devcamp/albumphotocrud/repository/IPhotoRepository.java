package com.devcamp.albumphotocrud.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.albumphotocrud.model.CPhoto;

public interface IPhotoRepository extends JpaRepository<CPhoto, Long>{
    CPhoto findById(long id); //tìm theo photo id
    List<CPhoto> findByAlbumId(long id);//tìm danh sách photo theo album id
}
